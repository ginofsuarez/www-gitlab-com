---
layout: markdown_page
title: Product Stage Direction - Plan
description: "The Plan stage enables teams to effectively plan features and projects in a single application"
canonical_path: "/direction/plan/"
---

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Enable teams to effectively plan and execute work in a single application</b>
    </font>
</p>

<%= partial("direction/plan/templates/overview") %>

<%= devops_diagram(["Plan"]) %>

## Stage Overview

The Plan Stage provides tools for teams to manage their work. As an end-to-end DevOps platform, GitLab is uniquely positioned to deliver a planning suite that enables business leaders to drive their vision and DevOps teams to deliver value while improving how they work. In addition, the unification of the DevOps process allows GitLab to interlink data across every stage of development, from initial analysis, to planning, implementation, deployment, and monitoring.

### Groups

The Plan Stage is made up of three groups supporting all major categories needed to plan work for DevOps organizations, including:

* The Project Management group focuses on Issue Tracking and Boards to help teams plan, track, triage, and complete their work.
* The Product Planning group focuses on Epics and Roadmaps to help leaders align objectives and increase visibility into the work for all teams in their organization.
* The Certify group enables focuses on Requirements Management and Quality Management to help organizations document, trace, and control changes to agreed-upon requirements in a system. 

### Resourcing and Investment
The existing team members for the Secure Stage can be found in the links below:

* [Project Management](https://about.gitlab.com/handbook/engineering/development/dev/plan-project-management-be/)
* [Product Planning and Certify](https://about.gitlab.com/handbook/engineering/development/dev/plan-product-planning-certify-be/)

## 3 Year Stage Themes
<%= partial("direction/plan/templates/themes") %>

## 3 Year Strategy

In three years, the Plan Stage market will:
* Continue to shift from project to product and focus on outcomes instead of output.
* Continue to move away from command and control mentality and instead empower teams to determine how they can contribute toward business objectives.
* Embrace machine learning and automation within the Plan stage of the DevOps toolchain and lifecycle.
* Shift toward consolidation into a single platform for all stages of the DevOps lifecycle.

As a result, in three years, Gitlab will:
* Provide support for individual DevOps teams and entire organizations using scaled Agile frameworks.
* Allow GitLab to capture and tie metrics to work items to reflect business outcomes. 
* Support frameworks like OKRs that encourage bottom-up contributions. 
* Use downstream DevOps data for automation and machine learning to help teams improve their plans.
* Make it easy for non-Developer Personas to contribute and read and edit planning data in GitLab. 

## 1 Year Plan


### What We Recently Completed

* [Epic Swim Lanes](https://docs.gitlab.com/ee/user/project/issue_board.html#group-issues-in-swimlanes) - Segmenting an issue board by epics allows users to track progress against a higher-level strategic initiative. 
* [Epic Boards](https://docs.gitlab.com/ee/user/group/epics/epic_boards.html) - Enable users to visualize and refine all of your epics in one place, using a customizable, drag-and-drop interface that is easy for any teammate to understand and collaborate. Previous versions of GitLab required you to view and sort epics in a list to view the overall status.
* [Iterations](https://docs.gitlab.com/ee/user/group/iterations/) - Iterations are a way to track work over a period of time and allow teams to track velocity and volatility metrics. Iterations can be used in conjunction with [milestones](https://docs.gitlab.com/ee/user/project/milestones/#milestones) to track an issue planning period and release independently. [Iteration lists in Boards](https://docs.gitlab.com/ee/user/project/issue_board.html#iteration-lists), you can efficiently plan multiple iterations in a single view. 
* [Burnup charts and historically accurate reporting](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html) - Milestones and iterations now show a burnup chart that tracks the total daily count and weight of issues added to and completed within a given timebox. We also changed the way we track data to ensure that milestone and iteration charts remain historically accurate even after you've moved issues to another timebox.

### What We Are Currently Working On

* [Unified work item architecture and refactored/revamped UI/UX](https://gitlab.com/gitlab-org/gitlab/-/issues/332644) - A unified implementation for Work Items will reduce rework and allow us to iterate faster. We will expand Issues to allow us to accommodate the desired attributes and behaviors of [epics](https://gitlab.com/groups/gitlab-org/-/epics/6033) and [requirements](https://gitlab.com/gitlab-org/gitlab/-/issues/323779).
* [Adding additional new work item types](https://gitlab.com/gitlab-org/gitlab/-/issues/323404) - Customers will have additional ways to classify and report on their work. 
* [Automating the creation of iterations](https://gitlab.com/groups/gitlab-org/-/epics/5077) -  Reduce the amount of administrative work for teams by automatically creating iterations at the group level.  
* Improving performance will continue to be a top priority in the near term. The rapid growth of GitLab.com has uncovered the need for continue focus on [database and query optimization](https://gitlab.com/groups/gitlab-org/-/epics/5804).

### What's Next For Us

* [Velocity](https://gitlab.com/groups/gitlab-org/-/epics/435) - Velocity is a measurement based on a single team's historical rate of delivering work items that helps them understand how much they can reasonably expect to complete in a given upcoming iteration. Understanding throughput enables teams to set better commitments with stakeholders. 
* [Issue statuses](https://gitlab.com/groups/gitlab-org/-/epics/5099) - While labels have served as a method to represent status for some time, our customers have expressed the need to elevate important meta-data to have a first-class experience within issues. Issue statuses will provide better clarity about the current workflow step of an issue, reduce the configuration necessary to calculate cycle time, and enable us to more easily deliver an out-of-the-box planning experience that [works by default](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles). 
* [Saved views](https://gitlab.com/groups/gitlab-org/-/epics/5516) and [grid views](https://gitlab.com/gitlab-org/gitlab/-/issues/323095) - Boards, Lists, and Grids become views that are easily accessed and saved. This thread of work sets the stage for executive rollups.
* [OKRs and rollups](https://gitlab.com/gitlab-org/gitlab/-/issues/36775) - Enable OKRs, Initiatives, and other executive-level work items into critical views.

### What We're Not Doing
* GitLab will not provide extensive configuration options that complicate collaboration and increase the maintenance burden. Examples are:
   * Blocking users from editing issues due to field-level configuration.
   * Creation of state workflows with transition rules. 
   * Configuring screen field layouts.
* GitLab's requirements management capabilities will not compete with products like IBM DOORS. Requirements systems like IBM DOORS will continue to be the source of truth, with GitLab focusing on traceability to DevOps artifacts.  We will enable teams to break down, link requirements to tests and code, and export data. 
* Plan functionality will be generic enough to allow its use by many personas and use cases. However, we will not focus on optimizing for use cases beyond DevOps organizations. Example use cases that are out of scope include:
   * ITSM
   * HR
   * Legal

Please explore the individual [Category](https://about.gitlab.com/direction/plan/#categories) Direction pages for more information on 12 month plans.

## Target audience

GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩 - Targeted with strong support
* 🟨 - Targeted but incomplete support
* ⬜️ - Not targeted but might find value

### Today

1. 🟩 [Software Developers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟨 [Development Team Leads](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟨 [Application Development Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#dakota-application-development-director)
1. 🟨 [Product Managers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
1. 🟨 [Software Engineer in Test](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Product Designers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer)
1. 🟨 [DevOps Engineers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟨 [Security Operations Engineers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer)
1. 🟨 [Release Manager](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
1. 🟨 [Security Analysts](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
1. ⬜️ [Application Ops](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. ⬜️ [Platform Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. ⬜️ [Compliance Managers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)
1. ⬜️ [Systems Administrator](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

### Medium Term (1-2 years)

1. 🟩 [Software Developers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Development Team Leads](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Application Development Director](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#dakota-application-development-director)
1. 🟩 [Product Managers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
1. 🟨 [Software Engineer in Test](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Product Designers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer)
1. 🟨 [DevOps Engineers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟨 [Security Operations Engineers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer)
1. 🟨 [Release Manager](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
1. 🟩 [Security Analysts](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
1. ⬜️ [Application Ops](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. ⬜️ [Platform Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟨 [Compliance Managers](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)
1. ⬜️ [Systems Administrator](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

### Categories

<%= partial("direction/categories-content", locals: { :stageKey => "plan" }) %>


## Pricing

<%= partial("direction/plan/templates/pricing") %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "plan" }) %>

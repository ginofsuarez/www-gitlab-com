--
layout: handbook-page-toc
title: "TaNewKi Tips"
description: "Pre-Onboarding Information for our Ta"New"Ki team members"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## TaNewKi Tips

## <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> point 1
{: #tanuki-orange}

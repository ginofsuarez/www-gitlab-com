### Enterprise planning frameworks support 

Our vision is to build a Plan experience that aligns with our [configuration principles](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles). When you look across the Enterprise Agile Planning tool landscape, you'll find solutions with large customer bases that cover many methodologies and personas. Our competitors have built highly flexible solutions with many configuration options to accommodate the large array of use cases. Unfortunately, this makes these tools non-performant and challenging to administer, leading to customer dissatisfaction. 

There are now countless solutions that aim to make planning tools that teams love. For example, tools like Notion, Monday.com, and Asana simplify configuration and focus on user experience. We will follow a similar approach, but our differentiation will be an experience geared toward enabling DevOps organizations to optimize their [agile fluency](https://martinfowler.com/articles/agileFluency.html).

As examples, GitLab will provide:
* Out of the box functionality that allows DevOps teams to follow Scrum, Kanban, and Requirements-based development processes.
   * Work Items that represent discrete units of work and are typically used by single teams. Examples are Stories, Bugs, and Requirements.
   * Views to facilitate Plan workflows for personas that work in a single team, like Engineers, UX, Product, and Engineering Managers. Example workflows are iteration planning, status views for a single team, and retrospectives.
* Out of the box functionality that aligns to industry-standard program and portfolio management, including frameworks like [SAFe](https://www.scaledagileframework.com), [LeSS](https://less.works), and [Disciplined Agile](https://www.pmi.org/disciplined-agile).
   * Work Items that represent collections of units of work and are typically used by multiple teams. Examples are Features, Epics, Themes, and OKRs.
   * Views to facilitate Plan workflows for personas that work across teams, like Program Managers, Managers of Managers, and Executives. Example workflows are quarterly or program increment planning, status reporting across multiple teams, and retrospectives across multiple teams.
* Options to customize the data captured about work items and their status. We currently rely on labels for this information, but our customers have expressed the need for a more structured approach. A lightweight custom data solution in GitLab has the ability to enable a wide range of use cases such as prioritization models and CapEx reporting.


### Use DevOps data to guide planning

As an end-to-end DevOps platform, GitLab is uniquely positioned to deliver a planning suite that enables business leaders to drive their vision and empower their development teams to work efficiently. Our unification of the DevOps process allows us to interlink data across every stage of development, from initial analysis to planning, implementation, deployment, and monitoring. Today, engineering and product managers need to parse through information in multiple systems to know if problems will keep their teams from meeting their plans. The data is often hard to understand, so engineers' days are interrupted with requests for status updates. Executives rely on status reports that are created manually and often inaccurate. To fully solve this problem, DevOps data within Plan should be displayed so that it's easy to understand for users that are not developers.

As examples, GitLab will:
* Leverage data from other DevOps stages to provide context to non-technical users enabling them to derive the current status of work items.
* Produce reports like a Requirements Traceability Matrix that tie requirements to specific lines of code and test results.
* Flag work items that are at risk during an iteration based on [DORA key metrics](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html#devops-research-and-assessment-dora-key-metrics) and [Value Stream Analytics](https://docs.gitlab.com/ee/user/group/value_stream_analytics/).
* Incorporate data from [DORA key metrics](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html#devops-research-and-assessment-dora-key-metrics) and [Value Stream Analytics](https://docs.gitlab.com/ee/user/group/value_stream_analytics/) into retrospective workflows. 
* Alert team leaders that a high number of [incidents](https://docs.gitlab.com/ee/operations/incident_management/) will likely disrupt planned work during an iteration. 

### Automation and machine learning to improve quality of life

Managing issues across an organization can be a time-consuming and tedious endeavor. It's normal for organizations to have a large number of stale issues that require manual clean-up. Our focus will be to let automation do the lower value work like managing status changes and automating the movement of work items among workstations (queues) so the people doing the work can focus more on creating value and less on remembering the process. At GitLab, we dogfood our issue tracker and have implemented a considerable amount of [automation](https://gitlab.com/gitlab-org/gitlab-triage) to start down this direction. We should double down on this strategy, incorporate automation into our product and reduce the manual setup required to enable it. GitLab can also provide suggestions and nudges that reduce the number of actions required to complete common tasks.

GitLab recently [acquired UnReview which to expand our single platform with machine-learning capabilities](https://about.gitlab.com/press/releases/2021-06-02-gitlab-acquires-unreview-machine-learning-capabilities.html) and form our new [Applied Machine Learning](https://about.gitlab.com/direction/modelops/applied_ml/) team. The Plan Stage will actively work with the Applied Machine Learning team to improve [Workflow Automation](https://about.gitlab.com/direction/modelops/applied_ml/#categories) within Product Planning and Project Management.

As examples, GitLab will provide:
* Auto-assign issues based on previous assignments.
* Auto-close or suggest closing issues if downstream activity implies it. 
* Apply labels automatically based on historical issue data. 
* Change issue health if downstream DevOps activity implies it. 
* Surface, categorize and rank work items that may need a user's attention. 
* Optimize everyday manual actions like work item data entry or updates by providing real-time suggestions.

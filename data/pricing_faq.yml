groups:
  - group: "Payments and Pricing"
    questions:
      - question: Do you have special pricing for open source projects, educational institutions, or startups?
        answer: >-
          Yes! We provide free Ultimate licenses, along with 50K CI minutes/month, to qualifying open source projects, educational institutions, and startups. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/), [GitLab for Education](/solutions/education/), and [GitLab for Startups](/solutions/startups/) program pages.
      - question: What is a user?
        answer: >-
          User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
      - question: The True-Up model seems complicated, can you illustrate?
        answer: >-
          If you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also pay the full annual fee for the 200 users that you added during the year.
      - question: Is the listed pricing all inclusive?
        answer: >-
          The listed prices may be subject to applicable local and withholding taxes. Pricing may vary when purchased through a partner or reseller.
  - group: "CI/CD Pipeline Minutes"
    questions:
      - question: What are CI/CD pipeline minutes?
        answer: >-
          CI/CD Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners will not increase your pipeline minutes count and is unlimited.
      - question: What happens if I reach my minutes limit?
        answer: >-
          If you reach your limits, you can [manage your CI/CD minutes usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage),
          [purchase additional CI minutes](https://customers.gitlab.com/plans), or
          upgrade your account to Premium or Ultimate. Your own runners can still be used even if you reach your limits.
      - question: Does the minute limit apply to all runners?
        answer: >-
          No. We will only restrict your minutes for our shared runners (SaaS only). If you have a
          [specific runner setup for your projects](https://docs.gitlab.com/runner/), there is no limit to your build time on GitLab SaaS.
      - question: Do plans increase the minutes limit depending on the number of users in that group?
        answer: >-
          No. The limit will be applied to a group, no matter the number of users in that group.
      - question: Why do I need to enter credit/debit card details for free pipeline minutes?
        answer: >-
          There has been a massive uptick in abuse of free pipeline minutes available on GitLab.com to mine cryptocurrencies - which creates intermittent performance issues for GitLab.com users. To discourage such abuse, credit/debit card details are required if you choose to use GitLab.com shared runners. Credit/debit card details are not required if you bring your own runner or disable shared runners. When you provide the card, it will be verified with a one-dollar authorization transaction. No charge will be made and no money will transfer. Learn more [here](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/)
  - group: Subscriptions
    questions:
      - question: I already have an account, how do I upgrade?
        answer: >-
          Head over to [https://customers.gitlab.com](https://customers.gitlab.com), choose the plan that is right for you.
      - question: Can I add more users to my subscription?
        answer: >-
          Yes. You have a few options. You can add users to your subscription any time during the subscription period. You can log in to your account via the [GitLab Customers Portal](https://customers.gitlab.com) and add more seats or by either contacting [renewals@gitlab.com](mailto:renewals@gitlab.com) for a quote. In either case, the cost will be prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the additional licenses per our true-up model.
      - question: What happens when my subscription is about to expire or has expired?
        answer: >-
          You will receive a new license that you will need to upload to your GitLab instance. This can be done by following [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html).
      - question: What happens if I decide not to renew my subscription?
        answer: >-
          14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.
  - group: "What Is Included"
    questions:
      - question: Are GitLab Pages included in the free plan?
        answer: >-
          Absolutely, GitLab Pages will remain free for everyone.
      - question: How does GitLab determine what future features fall into given tiers?
        answer: >-
          On this page we represent our [capabilities](/company/pricing/#capabilities) and those are meant to be filters on our [buyer-based open core](/company/pricing/#buyer-based-tiering-clarification) pricing model. You can learn more about how we make tiering decisions on our [pricing handbook](/handbook/ceo/pricing) page.
      - question: How much space can I have for my repo on GitLab SaaS?
        answer: >-
          10GB per project.
      - question: Can I buy additional storage space for myself or my organization?
        answer: >-
          Yes, you can purchase additional storage for your group on the [GitLab customer portal](https://customers.gitlab.com/).
      - question: What features are not available on GitLab SaaS?
        answer: >-
          Some features are unique to self-managed and do not apply to SaaS. You can find an up to date list [on our why GitLab SaaS page](/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/index.html).
  - group: Licenses
    questions:
      - question: Can I acquire a mix of licenses?
        answer: >-
          No, all users in the group need to be on the same plan.
      - question: How does the license key work?
        answer: >-
          The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to run. During license upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of users. During the licensed period you may add as many users as you want. The license key will expire after one year for GitLab subscribers.
  - group: General
    questions:
      - question: Can I import my projects from another provider?
        answer: >-
          Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket. [See our documentation](https://docs.gitlab.com/ee/user/project/import/index.html) for all your import options.
      - question: Where is SaaS hosted?
        answer: >-
          Currently we are hosted on the Google Cloud Platform in the USA

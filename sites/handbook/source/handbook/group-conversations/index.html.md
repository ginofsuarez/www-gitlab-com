---
layout: handbook-page-toc
title: Group Conversations
description: >-
  Everything you need to know about GitLab's daily, recurring Group
  Conversations
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Group Conversations are recurring 25 minute meetings providing regular updates across all GitLab teams on a [rotating schedule](https://docs.google.com/spreadsheets/d/1SHJaGBwKcOCmmKfL0eyaTWNuTqPbQDQFbipAPM5SEfw/edit?usp=sharing). Functions with a [Key Review](/handbook/key-review) will also be scheduled for a Group Conversation with the exception of Legal. Team member attendance is optional for Group Conversations.

The [Executive Business Administrator Team, also known as EBA Team](/job-families/people-ops/executive-business-administrator/) are responsible for scheduling these events which should automatically appear within the GitLab Team Meetings Calendar from 08:00am to 08:25am Pacific Standard Time (PST) from Monday to Thursday. Some days might not have a Group Conversation. These are listed in the [schedule](https://docs.google.com/spreadsheets/d/1SHJaGBwKcOCmmKfL0eyaTWNuTqPbQDQFbipAPM5SEfw/edit?usp=sharing) as **Available**. If you would like to be added to the GC rotation, please post in the slack channel #group-conversations and tag @exec-admins.

All Team Members are invited to participate Group Conversations by adding questions and comments in the Group Conversation Agenda, a Google Doc linked in the respective calendar invite.

Non-confidential Group Conversations are streamed live and shared publicly to our [GitLab Unfiltered YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e). Confidential Group Conversations which are not streamed live or shared publicly to YouTube include Finance; Sales; Security and Partnerships.

Group Conversations at GitLab were previously known as Functional Group Updates (FGU) and were renamed because they're not presentations or updates but rather conversations - the term Functional Groups is now encompassed by the use of Departments.

In this video our CEO, Sid gives team members tips and tricks for hosting a FGU which are still relevant and applicable to Group Conversations.
<figure class="video_container"><iframe src="https://www.youtube.com/embed/MN3mzvbgwuc"></iframe></figure>Below is a guide to ensure everyone gets the most out of Group Conversations. If you have suggestions for improving the Attendee or Meeting Leader experience please create a Merge Request (MR) to update this page — [everyone can contribute](/company/strategy/#why)!

## Group Conversation vs. Key Review

It's common to wonder what the difference is between these two meetings. Generally, they cover the same or similar content, but for different audiences.

|  | Key Review | Group Conversation |
| --- | ---------- | ------------------ |
| **Audience** | Management | General GitLab Team |
| **Tone** | Formal | Informational |
| **Discussion** | High-context | [Low-context](https://about.gitlab.com/company/culture/all-remote/effective-communication/#understanding-low-context-communication) |

The Key Review is intended for senior experts to ask questions. The Group Conversation is open to the entire GitLab team to ask questions. We have both to give the senior experts enough time to ask questions since the Group Conversations can have 100+ attendees. Both are also a good place for sharing news and explaining initiatives. The same deck must be used for both calls. Here's a brief discussion of why we have both meetings, and how they can operate effectively:
<figure class="video_container"><iframe src="https://www.youtube.com/embed/tLOMA3663XM"></iframe></figure>

## Defining Terms

### Host

The Host of a Zoom call is the person leading the conversation, usually the [DRI](/handbook/group-conversations/#schedule--dri), subject matter expert and overseer of the slides created for a call (another term for the role is Presenter).

Questions will generally be directed to the Host, and they will usually answer them unless they feel that another team member has more information or expertise to offer. The Host/Presenter makes sure that questions are asked in the order they are listed in the agenda document.

The "Host" term can be confusing as Zoom has a specific definition for Host pertaining mainly to a permission level, which in practice GitLab does not assign to the term. _More information on Zoom's definition of "host" is explained in the Moderator section below_.

Before you host your first Group Conversation, please be sure complete this [Group Conversation Training Issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/group-conversation-host.md) which will support you in your preparations.

### Moderator

The Moderator of a Zoom call is the person that has Zoom's _Host_ level permission. Host level permission in Zoom permits streaming a call to YouTube, starting or ending a recording and giving co-hosting permissions to others.

The Moderator of a Zoom call is the person who is in charge of starting the call i.e. initiating the streaming to YouTube; updating the stream from public to private if a sensitive topic is mentioned, and keeping the meeting running efficiently and on time so that it does not go over the Quick Meeting time frame.

## For Attendees

Attendance of Group Conversations is optional - if you are unable to attend a Group Conversation at its scheduled time, don't let that hold you back! Watch the latest Group Conversation on the GitLab Unfiltered YouTube Channel. Alternatively Public Group Conversations can also be found in the [GitLab Unfiltered Podcast](/blog/2019/07/03/group-conversation-podcast/).

If you have questions around an upcoming Group Conversation or would like to start a discussion, please be sure to bring it to the `#group-conversations` Slack channel and @tag the presenter!

1. Please enter your questions or comments on the linked Google Doc. Preface your question with your full name (first and last name) because there might be other people with your first name on the call and it's helpful to newcomers if they are distinguished. Remember [First Post is a Badge of Honor](/handbook/communication/#first-post-is-a-badge-of-honor).
1. Keep in mind that Group Conversations are live streamed and shared publicly and that it's okay to opt out of using your name due to safety and privacy concerns.
1. Please do not include customer names in your questions/comments.
1. Be ready to ask your question out loud as it comes up in the queue.
1. When speaking on the call, please say your name first, to help others know who is speaking.
1. Please don't use the Zoom chat to add information but add them to the doc so people reading the agenda after the call can also benefit.
1. Not everything has to be a question. If you have a comment, bias to putting it into the Google Doc so that those who weren't able to attend the meeting live can see what you had to say (and any responses that arose from it).
1. Thanking and recognizing people is very important.
1. You can ask someone to present a slide to get more context.
1. It's okay to add a question to the end of the queue in the Google Doc as the conversation is taking place.
1. You can contribute taking notes to transcribe the answers during the conversation. Thanks!
1. Make sure there are at least 5 empty items in the list (that just contain a space) for people to add new questions.

## For Meeting Leaders

### Scheduling and Selecting an Alternate Host

Calls are scheduled by the [EBA Team](/job-families/people-ops/executive-business-administrator/). If you need to reschedule, please _switch_ your presenting day with another Group Conversation leader, by asking another leader in the #group-conversations channel publicly. Please also @ mention @exec-admins so they are aware. The Executive Business Administrator team is not responsible for finding a replacement for your day. If you've agreed to switch, the EBA DRI will do the following:

- Go to the _GitLab Team calendar_ invite
- Update the date of your and the other invite to be switched
- Choose to send an update to the invitees
- _If prompted_ with the questions to update 1 or all events, choose to only update this event
- Update the GC [schedule](https://docs.google.com/spreadsheets/d/1SHJaGBwKcOCmmKfL0eyaTWNuTqPbQDQFbipAPM5SEfw/edit?usp=sharing)

If you will be out of office on the day of your Group Conversation, or need a person other than the DRI listed in the [Schedule & DRI table](/handbook/group-conversations/#schedule--dri) to Host:

- Please update the Alternate Host line in the Group Conversation Agenda document.
- Please notify any of the Executive Business Administrator Team or your functional EBA in the #group-conversations Slack channel so that they may change the host in Zoom's settings.

If your Group Conversation falls on a day when you feel that a large percentage of the company may be OOO (for example, a Public Holiday observed by a number of countries). In such instances:

- In the #group-conversations channel, ask channel members if they agree that a Group Conversation should not occur on a particular date.
- If there is consensus, look at the Group Conversation [Schedule & DRI](/handbook/group-conversations/#schedule--dri). Find the next open slot and check on the GitLab Team Meetings calendar to be certain there is no meeting schedule on that day at the GC time (8am PT).
- Ping the EBA team (`@exec-admins`) in Slack in your thread and ask them to move your regularly scheduled GC to the open slot you have found. This will be a one-time move and all other instances of your GC will stay as scheduled.

All Zoom meetings now require passwords. If a Group Conversation event was created in Zoom prior to this rule, please ping the EBA team (`@exec-admins`) in Slack to update the GC event in Zoom and in the GitLab Team Meetings calendar.

### Preparation Tips

A little bit of preparation can go a long way in making the call worthwhile for everyone involved. People tend to spend at least an hour to prepare their updates.

#### Logistics

1. You can invite someone within your team to give the update. It doesn't need to be the team lead, but the team lead is responsible for making sure that it is given.
1. The host of the GC needs to make sure to add the link of the presentation to the Group Conversation Agenda Document invite at least 24 hours _before_ the call takes place. This allows team members to see the presentation, to click links, have [random access](https://twitter.com/paulg/status/838301787086008320) during the presentation, and to quickly scan the presentation if they missed it. Please also add who the speaker will be to the presentation and the invite. The EBA Team will ping the appropriate team member at the 24-hour mark if the event hasn't been updated yet.
1. Post links to the Key Review video (if applicable), Key Review Notes (if applicable), Group Conversation Agenda Document and the slides or video in the #whats-happening-at-gitlab channel 24 hours before the call.
1. Consider blocking off the 30 minutes before your scheduled to lead a Group Conversation

#### Presentation

Use presentations to document everything people should know about your group. These presentations are for attendees to review and generate questions from for the call. You can also record and upload a [YouTube video](/handbook/marketing/marketing-operations/youtube/) if there is additional context that would be beneficial to supplement with the slides. Functions with Key Reviews should not create a new deck for a Group Conversation but should use their [Key Review deck](/handbook/key-review/#group-conversations-and-key-review-metrics) for the Group Conversation.

**Please remember that you should NOT present during a Group Conversation.** Synchronous calls are for conversation and discussion. It is the responsibility of the attendees to prepare questions before the call to maximize value of time spent synchronously. If someone does start to present the slides in the group conversation everyone is encouraged to say: 'At GitLab we use meetings for conversation and not presentation, but please do consider recording a video for next time.' Expect to be interrupted if presenting more than a 15 second welcome statement.

There are three layers of content in a presentation:

- Data, this is the contents of the slide.
- Take away, this is the title of the slide, so use: 'migration 10 days ahead of schedule', instead of 'migration schedule estimates', the combined titles of your slides should make a good summary.
- Feelings, this is the verbal and non-verbal communication in the video feed, how you feel about the take away, 'I'm proud of the band for picking up the pace'.

1. Save time and ensure asynchronous communication by writing information on the slides. Many people will not be able to participate in Group Conversations, either live or recorded, but can read through the slides.
1. Slides with a lot of text that can be read on their own with lots of links are appreciated.
1. If you want to present, please consider [posting a recording to YouTube](/handbook/marketing/marketing-operations/youtube/) at least a day before the meeting. Link it to the Google Doc (including specifying whether the video is private or public), and mention it in the relevant slack channels.
1. Once a quarter, add a slide covering items being actioned from the engagement survey.
1. Use this [slide deck](https://docs.google.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing) as template to your presentation. Presentations should allow editing (preferred) or commenting from everyone at GitLab.

##### Examples of good presentations

- [Strategic Marketing Group Conversation 2020-01-22](https://docs.google.com/presentation/d/1xSxP_AztHHtLcYFd12Mf5yUIMVy9BU6QZnSebrlO7oo/edit): This has lots of links and a [supporting YouTube presentation](https://www.youtube.com/watch?v=2RfNsIJ7lPM).

#### Cancellation or Open Slot

We try not to cancel a GC.
We prefer to have half-prepared GCs over delaying them, but occasionally there may be a last-minute cancellation or an open time slot on the calendar.
If a Group Conversation is cancelled or if there is an opening in the schedule, the EBA Team should ask a member of the E-Group to host a Pop-up AMA.
These should be called Pop-up AMAs so it's clear to team members that the AMA was added to the calendars solely as a function of a GC cancellation.

#### Additional Consideration for Functional Presentations

[Groups](/handbook/key-review/#invitees) that also have [Key Reviews](/handbook/key-review/) should repurpose material created for their last Key Review in the Group Conversation immediately following the Key Review. This is to give the broader team exposure to progress against key metrics and objectives that are key to the business and to reduce content duplication. Groups with [Key Reviews](/handbook/key-review/) will have a monthly GC.

### 24 Hours Before the Call

1. Ensure the slide deck, any handbook or document links, and the optional video are visible to all of GitLab
1. Post in #whats-happening-at-gitlab on Slack if there is a video for the group conversation that you'd like people to watch beforehand. Please @ mention `Cheri Holmes` when posting

### 30 Minutes Before the Call

1. Open the questions Google Doc linked from the invite and skim the questions to get a sense of what you can expect.
1. Enable “Show document outline” under the View menu to navigate the document more easily.
1. Improve the hygiene of the questions doc to use numbered lists instead of bulleted lists, so you can refer to questions by number if needed later.
1. Reduce distractions for yourself and the attendees by:
    - having the presentation open in its own new browser window, and only sharing that window in Zoom.
    - switching off notifications (from Slack, email, etc.). On Mac, in the notification center in the upper right hand corner, click on Notifications, select Do Not Disturb (sometimes you need to scroll up to see the option).
1. The Moderator of the call may advise the Host that the conversation needs to be recorded via Zoom. As the Host, when hearing the automated "This meeting is being recorded", please note that you can start talking immediately and do not need to wait for the automated message to complete.

## For Moderators

The `EBA` team are responsible for moderating the following calls on a rotational basis:

- AMAs (If requested with sufficient notice as per the instructions detailed [here](/handbook/group-conversations/#request-an-ama)
- Group Conversations where an e-group member is the DRI
- CEO 101 (Introductions and Livestream)

The teams will assist with moderating calls where the audience is applicable to all GitLab team members. In the event that a separate meeting needs to be scheduled on the team calendar with only a specified invitee list, the host or the EBA for the host's organization will be responsible for moderating the call.

The `Host` is responsible for starting the livestream or designating one of their team members to start the livestream for the following calls:
- Group Conversations in the Product and Engineering Organization where an e-group member is not the DRI
- AMAs in the Product and Engineering Organization where an e-group member is not the DRI

### Preparing for the Group Conversation

All Group Conversations (Public and Private) will be streamed to YouTube within the [GitLab Unfiltered](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#channels) channel. Guidelines around streaming including initiating the livestream and troubleshooting should you hit a snag can be found on the [YouTube Uses and Access](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#starting-a-recorded-video) page.

Additional links which may be useful when preparing to moderate a call include:

- [The YouTube Livestreaming Introduction](https://support.google.com/youtube/answer/2474026?hl=en)
- [The YouTube Tutorial on Setting Up a Livestream](https://support.google.com/youtube/answer/2853700?hl=en)

### Prior to the Call

1. Ping the [DRI](/handbook/group-conversations/#schedule--dri) of the Group Conversation you will be moderating in the `#group-conversations` Slack channel at least 24 hours prior to confirm whether they will be hosting or have nominated another team member to do so.
1. If the host for the particular Group Conversation you are moderating has not hosted a call of this nature before, be sure to assign a [Group Conversation Training Issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/group-conversation-host.md) to them to support their preparation for the call.
1. Be sure to confirm ahead of time whether this will be a [Public or Private Stream](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#never-call-it-a-livestream-or-just-a-recording) as you will need to mention this at the beginning of the call.
1. Review the `Group Conversation Agenda` to ensure there are 15 numbered lines below the Date; Topic; Host Name; and Slide Deck Link - this is where team members will be able to insert any questions they may have for the host.
1. Log into Zoom `5-minutes prior` to the start of the Group Conversation using `EBA Zoom Account' if you are successfully logged into Zoom as the GitLab Moderator the Tanuki Logo will be visible in the upper right hand corner of your Zoom screen.
1. Ensure that you as the moderator hold the Zoom hosting rights as you will not be able to livestream the call otherwise i.e. co-hosting rights are not sufficient so please be sure to request a handover from the DRI using the `manage participants feature` if necessary.
1. Ensure the clock on your computer is enabled to include seconds by clicking the Apple icon in the upper lefthand corner of your screen, selecting `System Preferences`, opening `Date and Time` preferences, and selecting `Display the Time with Seconds`.

### During the Call

1. It is important to start Start the Group Conversation at the exact time indicated within the calendar invite; it is at this point which you will start the livestream using the following [guidelines](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#starting-a-livestream).
1. If an error which reads `Please grant necessary Privilege for Livestreaming` pops up, click on the option that appears beside the GitLab Unfiltered account in the middle of the page reading `Not Me` - you will then be prompted to sign in again, please do so and click `Go Live` afterwhich Zoom should indicate `Live on YouTube` in the upper left hand orner.
1. Be sure to either close or mute the YouTube page to prevent an echo.
1. In the event that you are unable to stream to YouTube for any reason, inform the Host that you will record the conversation via Zoom and upload the video to YouTube after the conversation has ended. In addition to this be sure to post the YouTube link to the #group-conversations Slack channel once it has been uploaded.
1. Check whether the host has included a pre Group Conversation video link in the agenda doc and that it has been specified as Private/Public.

### Concluding the Call

1. Post notes within the Zoom chat at both five and two minutes remaining mark - this will ensure everyone is aware of the time left within the call and encourage team members to keep the conversation on track.
1. At time notify the Host that the meeting should be concluded allowing time for all participants to wrap up the active discussion and thanking them for their time and collaboration.
1. End the Group Conversation by clicking `End Meeting` and then `End Meeting for All` within Zoom - making this selection should end both the call and the stream - should this not be the case it is more than likely because an outdated version of Zoom is being used and the moderator will need to log directly into YouTube Studio and manually end the stream.
1. Once the call ended, audit the YouTube video for any issues such as another stream on the video or errant transcript in the description. If you identify any issues with the video, immediately reach out to [Security](/handbook/engineering/security/#engaging-the-security-on-call) for support.
1. Once the stream has ended, copy and paste the YouTube link to the Group Conversation agenda. This will allow easy access to the stream without having to search for it in the GitLab Unfiltered channel.
1. If livestreaming was not possible manually upload the video to YouTube using these [guidelines](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube).

<figure class="video_container">
 <iframe src="https://youtube.com/embed/yuFlj8Zv7vQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Additional Notes (Host)

1. As the Group Conversation and Zoom Host, when hearing the automated "This meeting is being recorded", please note that you can start talking immediately and do not need to wait for the automated message to end.
1. Right now, everyone within GitLab i.e. all team members are invited to each call, we'll invite the wider community later.
1. Make sure there is a list of 15 numbered items that only contains a space, this will make it easier to add things.
1. Join the call 2-3 minutes early to ensure you're ready when it starts.
1. If the call is to be streamed, the moderator will follow the script as directed in the [streaming the Call](/handbook/group-conversations/#during-the-call) section on this page.
1. When the meeting starts introduce yourself and say a few words about what this call is about. For example, "Hello everyone, I'm Diane and I lead the [team]. I am looking forward to answering your questions about our [group name] group."
1. Do not present your slides. Invite the first person to verbalize their question, respond and when relevant mention a slide number. This is similar to what we do during [board meetings](/handbook/board-meetings/).
1. Remember to only share the slides if illustrating something, otherwise let the speakers be visible in `Speaker View` setting on Zoom.
1. It is okay to wait until at least two questions have been entered before starting to answer questions, and allow some time for folks to read the slides at the start of the call.
1. If someone can't verbalize their question (not in call, driving, audio problems) read it aloud for people watching only the video.
1. Tone should be informal, like explaining to a friend what happened in the group last month, and shouldn't require a lot of presentation.
1. It's the responsibility of the team members of the group to ensure the content is distributed, this includes ensuring appropriate notes are taken in the [Group Conversation Agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit)
1. This meeting is scheduled to be 25 minutes long. Please keep an eye on the clock and end the meeting on schedule. This meeting must end no later than 29 minutes after the hour.
1. It is appropriate to end the call early if there are no more questions. We aim for results, not for spending the time allotted. Please avoid the temptation of presenting slides to fill time.
1. Don't do a countdown when you're asking for questions, people will not ask one. Just end the call or even better say: we'll not end the call before getting at least one question.
1. If there are more questions or a need for longer conversation, mention on what chat channel the conversation can continue or link to a relevant issue.
1. If an attendee experiences poor wifi or loses audio while speaking on a call sometimes they can't hear you if you tell them. In that case it's encouraged to talk over them, mute them, and pro-actively move the next agenda item. Nobody wants to have other people to waste their time waiting for them while they are not aware.

## Schedule & DRI

There is a [rotating schedule](https://docs.google.com/spreadsheets/d/1SHJaGBwKcOCmmKfL0eyaTWNuTqPbQDQFbipAPM5SEfw/edit?usp=sharing) with each Department having a conversation on a regular interval.
We usually do not have Group Conversations or Company calls during the last two weeks of December. We also tend to cancel these during [Contribute](https://about.gitlab.com/events/gitlab-contribute/).

On 2021-04-19, we'll move our Group Conversations to a new schedule to ensure [functions with Key Reviews](/handbook/key-review/#invitees) will have monthly Group Conversations within 10 days following their Key Review. They are expected to largely leverage their Key Review material. Other groups will have one meeting in the eight week cycle. Please note: the freqency of Group Conversations does not recur on a specific cadence, but rather an estimated time (ex: the Strategic Marketing GC happens every other month, but does not happen specifically on the 4th Wednesday every other month). Groups Conversations without Key Reviews can only be swapped with other Groups without Key Reviews, or scheduled during an open day that is available as noted on the [schedule](https://docs.google.com/spreadsheets/d/1SHJaGBwKcOCmmKfL0eyaTWNuTqPbQDQFbipAPM5SEfw/edit?usp=sharing).

| Group Conversation | DRI | Key Review? | GC Frequency |
| ------------------ | --- | ----------- | ------------ |
| [CRO](/handbook/sales/) | Michael McBride | Yes | Monthly |
| [Finance](/handbook/finance/) | Brian Robins | Yes | Monthly |
| [Marketing](/handbook/marketing/) |  Craig Mestel | Yes | Monthly |
| [People](/handbook/people-group/) | Wendy Barnes | Yes | Monthly |
| [Product](/handbook/product/) | Scott Williamson | Yes | Monthly |
| [Infrastructure](/handbook/engineering/infrastructure/) | Steve Loyd | Yes | Monthly |
| [Support](/handbook/support/) | Tom Cooney | Yes | Monthly |
| [Security](/handbook/engineering/security/) | Johnathan Hunt | Yes | Every other month |
| [Security](/handbook/engineering/ux/) | Christie Lenneville | Yes | Every other month |
| [Development](/handbook/engineering/development/) | Christopher Lefelhocz | Yes | Every other month |
| [Quality](/handbook/engineering/quality/) | Mek Stittri | Yes | Every other month |
| [Strategic Marketing](/handbook/marketing/strategic-marketing/) | Tye Davis | No | Every other month |
| [All-Remote](/handbook/marketing/corporate-marketing/all-remote/) | Darren Murph | No | Every other month |
| [Digital Experience](/handbook/marketing/inbound-marketing/digital-experience/) | Michael Preuss | No | Every other month |
| [General](/handbook/ceo/) | Sid Sijbrandij | No | Every other month |
| [Brand Activation](/handbook/marketing/corporate-marketing/brand-activation/) | Becky Reich | No | Every other month |
| [Inbound Marketing](/handbook/marketing/inbound-marketing/) | Danielle Morrill | No | Every other month |
| [UX Research](/handbook/engineering/ux/ux-research/) | Sarah Jones/Adam Smolinski | No | Every other month |
| [Community Relations](/handbook/marketing/community-relations/) | David Planella | No | Every other month |
| [Revenue Marketing](/handbook/marketing/revenue-marketing/) | Evan Whelchel | No | Every other month |
| [Alliances](/handbook/alliances/) | Mayank Tahilramani | No | Every other month |

## Request a new Group Conversation

To request a new Group Conversation, please ping @exec-admins in the slack channel #group-conversations and they will assist you in finding an open slot.

## Request an AMA

- The EBA Team manages the GitLab Team Meetings calendar and their events.
- The EBA Team moderate Group Conversations and AMAs.
- As part of designated Bring Your Family and Friends to Work events, some AMAs will be open to family members (including kids) and friends. These AMAs will be a subset of AMAs that do not contain sensitive data (sensitive data includes: information we usually don't share publicly). These events will be publicized ahead of time through #whats-happening-at-gitlab. The calendar invite for these AMAs will specify that they are "open to family and friends." To arrange this, reach out to your EBA team member and ask them to update the calendar invite to show "open to family and friends". In the event your EBA is OOO, please reach out in slack to '@exec-admins' to add it.

We suggest each person attend via their own device to avoid [hybrid meetings](/handbook/communication/#hybrid-calls-are-horrible). We encourage you to follow up with your guest after to debrief and share call impressions. For this it might be good to be in the same room but only if you are sure not to get echo's.

### Steps to Request an AMA

1. To request an AMA be put on the GitLab team meetings calendar, the requestor should find a date and time on the GitLab Team Meetings calendar that works for the AMA host.
1. The requestor will slack [the EBA](https://about.gitlab.com/handbook/eba/)for their department in the `#group-conversations` Slack Channel and provide the following information:
    - Title of AMA
    - Public or Private Livestream
    - Date & Time (YYYY-MM-DD PST)
    - Host
    - Moderator
    - Agenda Link
    - Attendees (specify if calendar invitations go to specific teams or to everyone@gitlab.com)
    - Confirm if this call is "open to friends and family"

- The EBA will create the event in the EBA Zoom account and add it to the GitLab team meetings calendar. The EBA will add the AMA document link in the description and invite the specified attendees to the meeting. The EBA will add Alternate Hosts if applicable. 

## Archiving Content

If an agenda document becomes longer than 10 pages, the People Experience or People Specialist team member assigned to moderate a meeting will archive the content that is over the first 10 pages. All agenda docs should be checked and archived as needed before the meeting date and time.

- Create a Google doc with the same permissions as the doc from which you will be archiving content.
- Name the new doc the same title as the original, with the word `Archive` at the end.
- Create a link at the top of the original doc titled `DOC NAME Archive` and link to the Archive doc.

## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions and discussions live or in chat

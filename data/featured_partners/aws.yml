title: GitLab on AWS
canonical_path: /partners/technology-partners/aws/
seo_title: GitLab on AWS
description: Unblock your process with a complete DevOps platform to build, test, and deploy on AWS.
title_description: Unblock your process with a complete DevOps platform to build, test, and deploy on AWS.
logo: /images/logos/aws-logo.svg
logo_alt: Amazon Logo
body_top_title: Deploy anywhere with everyone
body_top_description: GitLab is a DevOps platform with bring-your-own-infrastructure flexibility. From the on-premise to cloud, run GitLab on AWS and deploy to your workloads and AWS infrastructure using a single solution for everyone on your pipeline.
body_top_cta_1_text: Read the whitepaper
body_top_cta_2_text: Start your free trial
body_top_cta_img_1: "/images/topics/featured-partner-aws-1.png"
body_top_cta_img_2: "/images/topics/featured-partner-aws-2.png"
body_top_cta_1_url: /resources/whitepaper-deploy-aws-gitlab/
body_top_cta_2_url: https://aws.amazon.com/marketplace/pp/GitLab-GitLab-Ultimate/B07SJ817DX
body_features_title: Develop better cloud native applications faster with GitLab and AWS
body_features_description: GitLab collapses cycle times by driving efficiency at every stage of your software development process  –  from idea to deploying on AWS. GitLab’s complete DevOps platform delivers built-in planning, monitoring, and reporting solutions plus tight AWS integration for any workload. 
body_quote: The feature set with GitLab on-premise was more advanced than GitHub, and we saw the pace and development [of GitLab] moving faster with a community that was active in delivering and contributing.
body_quote_source: Eric Labourdette, Head of Global R&D Engineering Services at Axway
feature_items:
  - title: "Rapid collaboration"
    description: "Contribute with purpose. Version control and collaboration reduce rework so happier developers can expand product roadmaps instead of repairing old roads. "
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
  - title: "Reliable workflows"
    description: "Deploy unstoppable software with DevSecOps. Automated workflows increase uptime by reducing security and compliance risks on AWS. "
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
  - title: "Repeatable results"
    description: "Stay in the game to win it. Increase market share and revenue when your product is on budget, on time, and always up. "
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
body_middle_title: "Get started with GitLab and AWS joint solutions"
body_middle_description: >-
  As a certified AWS Advanced Technology Partner with DevOps Competency, GitLab CI/CD is a proven model for customer success with the leading cloud platform. AWS customers can choose from two deployment options: **GitLab self-managed** and **GitLab SaaS**. 


  Install, administer, and maintain your own GitLab instance that runs on everything from bare metal, VMs, and containers on AWS with GitLab self-managed. GitLab SaaS requires no installation, so you can sign up and get started quickly. 
body_middle_items:
  - title: "Amazon Elastic Compute Cloud (EC2)"
    description: "Amazon EC2 provides scalable AWS cloud computing capacity. GitLab scales jobs across multiple machines. When used together, GitLab on EC2 can significantly reduce infrastructure costs."
    icon: "/images/icons/icon-1.svg"
    link: "https://docs.gitlab.com/ee/install/aws/"
  - title: "AWS Fargate"
    description: "With one click on GitLab, AWS Fargate enables scalable serverless container deployments. Organizations migrate to Fargate to optimize compute resources and save on infrastructure costs. Fargate works with an AWS stack that includes ECS or EKS."
    icon: "/images/icons/icon-2.svg"
    link: "https://about.gitlab.com/customers/trek10/"
  - title: "Amazon Elastic Kubernetes Services (EKS)"
    description: "AWS Elastic Kubernetes Service (EKS) is a managed Kubernetes service. GitLab CI/CD offers integrated cluster creation for EKS. EKS is the only Kubernetes service that lets existing AWS users take advantage of the tight integration with other AWS services and features. GitLab also supports Amazon EKS-D."
    icon: "/images/icons/icon-3.svg"
    link: "https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to/"
  - title: "AWS Lambda"
    description: "AWS Lambda is a computing service that runs code in response to events and automatically manages the computing resources required by that code. GitLab supports the development of Lambda functions and serverless applications with AWS Serverless Application Model (AWS SAM) and GitLab CI/CD."
    icon: "/images/icons/icon-4.svg"
    link: "https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html"
  - title: "AWS Elastic Container Service (Amazon ECS)"
    description: "AWS Elastic Container Service (Amazon ECS) is a container management service. Save time when you run AWS commands from GitLab CI/CD and automate docker deployments with GitLab’s CI templates."
    icon: "/images/icons/icon-5.svg"
    link: "https://docs.gitlab.com/ee/ci/cloud_deployment/#deploy-your-application-to-the-aws-elastic-container-service-ecs"
  - title: "Windows .Net on AWS"
    description: "GitLab enables CI/CD for Windows .Net applications on AWS. Automatically deploy containerized applications, including serverless resources, with GitLab on Lambda or Fargate."
    icon: "/images/icons/icon-6.svg"
    link: "https://youtu.be/_4r79ZLmDuo"
featured_video_title: "Opening Keynote: The Power of GitLab - Sid Sijbrandij"
featured_video_url: "https://www.youtube-nocookie.com/embed/xn_WP4K9dl8"
featured_video_topic: "GitLab Commit Virtual 2020"
featured_video_more_url: "https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/featured"
benefits_title: Discover the benefits of GitLab on AWS
resources:
  - title: How to deploy on AWS from GitLab
    url: /resources/whitepaper-deploy-aws-gitlab/
    type: Whitepapers
  - title: Deploy AWS Lambda applications with ease
    url: /webcast/aws-gitlab-serverless/
    type: Webcast
  - title: CI/CD with Amazon EKS using AWS App Mesh and Gitlab CI
    url: https://aws.amazon.com/blogs/containers/ci-cd-with-amazon-eks-using-aws-app-mesh-and-gitlab-ci/
    type: Blog
  - title: AWS DevOps Partner Summit
    url: https://jamesbland-share.s3.us-west-2.amazonaws.com/SI_Summit2020/partners/gitlab.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAXFO5CEICVIYGQVBU%2F20201019%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201019T222755Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=a40d1ae3e765f261484a1ecbd9be5e7633cf48c8db2c7c398e5a1595f0d9b6b0
    type: Webcast
  - title: Up-level your AWS infrastructure automation with GitOps
    url: https://www.youtube.com/watch?v=QgD8Jz22TXg
    type: Webcast
  - title: How GitLab accelerates workload deployments on AWS
    url:  https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/uploads/59d1390557bb304e7401443c4e710c0f/gitlab-aws-ci_t-campaign-infographic-01.pdf
    type: Whitepapers
  - title: Deploy your software to AWS you get the best DevOps tooling running on the best cloud infrastructure
    url:  https://about.gitlab.com/resources/downloads/GitLab_AWS_Solution_Brief.pdf
    type: Whitepapers
  - title: Northwestern Mutual deployed GitLab Runners in AWS to manage demand for capacity and cost
    url: https://www.youtube.com/watch?v=K6OS8WodRBQ
    type: Video
  - title: WagLabs is achieving the impossible by using self-hosted GitLab on AWS to power its CI/CD
    url: https://www.youtube.com/watch?v=HfEl9GXZC0s
    type: Video 
  - title: Ask Media Group made the shift from on-prem data centers to the AWS cloud by leveraging GitLab Runners
    url: https://about.gitlab.com/webcast/cloud-native-transformation/
    type: Webcast
  - title: GitLab + HeleCloud - Leveraging GitOps to improve your operational efficiencies with AWS
    url: https://www.youtube.com/watch?v=eiZUVPeyktI&t=391s
    type: Webcast
cta_banner:
  - title: How can GitLab integrate with your AWS infrastructure?
    button_url: https://about.gitlab.com/sales/
    button_text: Talk to an expert

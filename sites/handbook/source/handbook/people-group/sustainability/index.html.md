---
layout: handbook-page-toc
title: Sustainability at GitLab
description: Strategies and details on GitLab's sustainability efforts
twitter_image: "/images/opengraph/handbook/handbook-sustainability.png"
twitter_image_alt: "Sustainability focused handbook illustration with the GitLab Tanuki logo and a globe on the cover of the handbook"
twitter_site: "gitlab"
twitter_creator: "gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sustainability at GitLab

As GitLab's reach in the global developer community grows, so does our impact on the environment. While we intentionally operate within our values as part of our operations, we have not yet understood how to apply our values to our business's impact on the environment. This handbook page will document our growing body of work in aligning our sustainability initiatives with our values. 

## GitLab's mission can't succeed without sustainability 

[GitLab's mission is to create a world where everyone can contribute](https://about.gitlab.com/company/strategy/#mission). When everyone can contribute, consumers become contributors, and we greatly `increase the rate of human progress`.

_How_ and _if_ we can increase the rate of human progress is limited by humans' ability to focus on contributing. Today, we see a reduction in contributions when someone is located where extreme weather events like floods or wildfires occur. Tomorrow, we may see a drop in contributions due to dramatic shifts in weather patterns. 

The wider community's total contributions are at risk of permanent reduction when large groups of people are required to migrate due to climate change. [This is already beginning to happen today](https://www.nytimes.com/interactive/2020/07/23/magazine/climate-migration.html). 

If we don't hold ourselves accountable for sustainability, GitLab will continue to attack our mission, reducing the possibility of long-term success.

## We recognize the truth

We'll need to recognize some truths that outline where we are today for our values to drive us to the proper emissions reduction solutions.

#### The internet is a significant carbon emitter

What's out of sight is usually out of mind. So we must recognize the truth: The emissions of the internet (gadgets, systems required to operate, and the internet itself) accounts for approximately 3.7% of global greenhouse emissions - **similar in size and scope to the emissions produced by the airline industry**. _source: [BBC](https://www.bbc.com/future/article/20200305-why-your-internet-habits-are-not-as-clean-as-you-think)_

#### It's our responsibility, and we must be held accountable

- Emissions caused by GitLab's business operations are GitLab's emissions to reduce now and eliminate soon.
- Waste produced by our business operations is GitLab's waste to process.
- Inequity produced by our business operations is GitLab's inequity to correct.

Any unintended negative side effect of GitLab's business operations is GitLab's responsibility to take action on and to correct.

By beginning work on internal sustainability initiatives, we're focusing our efforts on holding ourselves accountable rather than waiting until required through cultural or regulatory means.

We hope to advocate for this position in our strategies and the global sustainability movement's greater context.

#### GitLab will eventually be required to operate a sustainable business

Either by cultural or governing body regulations, most businesses won't operate without a sustainable business strategy and public reporting on their efforts. We choose to organize our strategy before these regulations take effect and hope to advocate for these kinds of policies in the global sustainability movement's greater context.

## What we believe

We still have a lot to learn. But what we know today helps us to define how we'll move our sustainability initiatives forward.

#### Being All-Remote is an advantage and a unique challenge for sustainability work
GitLab has eliminated a significant amount of emissions by not having a daily commute to work. However, we've also complicated our ability to account for the different types of electricity use. In a co-located office in a traditional company, there is one electric bill. But in an all-remote company in over 60 countries, emissions generated from team member electricity use get more challenging to reduce or eliminate as the team grows. 

Emissions caused by team member electricity use for GitLab's business operations are the responsibility of GitLab to reduce now and eliminate soon. 

#### We must act as a fiduciary for our Team Members, Customers, Community, and Planet

While most sustainability initiatives focus on things we can do to help the planet, the outcomes are intended on helping people. To take the lives and wellbeing of our team members, customers, and the community seriously, we need to act as a fiduciary for the planet.

#### We have a lot of work to do

Depending on how you look at the world's companies and their commitments to sustainability, GitLab may seem behind on the times. Many of our partners and competitors have already made public commitments to operating more sustainably. But this is also an excellent gauge to research the commitment levels across the industry and make sure that GitLab's commitments don't just sound nice but ladder to actual targets.

#### We must center our actions on equity

While diversity is a critical GitLab value, we must understand that we are not adequately supporting our team members, customers, community, or the planet without intentionally working equity into our sustainability initiatives. 

#### Reduce now, eliminate soon

We don't know what our philosophy on sustainability will be yet. We don't have more information to share on what commitments we'll soon make. But we do know that we'll need to focus on reducing our emissions now to begin to eliminate those emissions soon. 

## Sustainable projects already in motion

While not organized under a sustainability banner, the following projects have been driven by or considered sustainability.
- [Organizing existing work that enables our marketing website to be more energy efficient](https://gitlab.com/gitlab-com/marketing/inbound-marketing/marketing-website/-/issues/110)
- Contribute, our annual team member in-person event, considers carbon offsets for travel, which began in 2020.
- [LinkedIn Learning course for GitLab team members to understand their carbon impacts](https://gitlab.com/gitlab-com/sustainability/-/issues/8)
- This handbook page also serves as our project for organizing sustainability at GitLab

## What comes next?

- Develop a purpose, vision, mission, objectives, strategies, and principles that guide GitLab's point-of-view on sustainability
- Identify that not addressing sustainability needs is [one of GitLab's biggest risks](https://about.gitlab.com/handbook/leadership/biggest-risks/).
- Identify "sustainability champions" across the company and through different teams to consider more sustainable practices for their outcomes
- Continue the work with our marketing website energy efficiency project
- Get more team members talking about sustainability topics in the #sustainability Slack channel

## What can you do?

#### Team Members

- Research ways sustainability could positively impact your line of work
- Contribute to our monthly releases, focusing on features and stability improvements that aid efficiency
- Join the #sustainability Slack channel to gather with other team members interested in the topic
- Contribute to this handbook page. Does something you're working on aid in sustainability? Add it to the list of projects already in motion.
- Reach out to @wspillane on Slack or in the #sustainability slack channel if you'd like to discuss how you can get involved with sustainability efforts at GitLab or how the work you're already doing is contributing

#### The Community 

- [Contribute to GitLab development, focusing on features and stability improvements that aid in efficiency](https://about.gitlab.com/community/contribute/development/)
- Contribute to this handbook page

#### Partner

- Share your company's experiences in sustainability with us
- Help us to understand how your company got sustainability practices started 
- Consider contributing to this handbook page 
